@extends('layout')

@section('judul')
	About
@endsection

@section('konten')
<div class="container-about scroll">
	<div class="content">
		<center>
			<img class="rounded-image" src="https://i.pinimg.com/originals/d8/f0/ad/d8f0ad5f5ff2385b46bcb736f504a3dd.jpg" alt="Foto Ni Putu Anisa Marta Widyasari" width="160px" height="=140px">
		</center>
		<br>
		
			<h1 align="center"><span>ABOUT <font color="blue">ME</font></span></h1>
			<br>
			<p align="center">
			HAI!!! aku Ni Putu Anisa Marta Widysari, biasa dipanggil Anisa. Aku lahir di Denpasar, 31 Juli 2001 dan sekarang usiaku adalah 20 tahun <br> 
			Saat ini aku sedang menempuh pendidikan di Universitas Pendidikan Ganesha, Jurusan Teknik Informatika, Prodi Sistem Informasi <br>
			Aku merupakan alumni dari SDN Tulang Ampiang, SMP Negeri 4 Denpasar dan SMA Negeri 7 Denpasar <br>
			Selama pandemi aku kerap melakukan hobby ku yaitu menonton film
		</p>
		<br><br><br>
		<center>
			<a class="sosmed" href="https://web.facebook.com/choi.anisa" target="blank" title="Halaman Profil Facebook Anisa"><img src="https://i.pinimg.com/originals/09/fd/ac/09fdacd934e0d30371be16f6a51ceb1b.png" height="40px" width="40px"></a>
			<a class="sosmed" href="https://www.instagram.com/anisamartaa/" target="blank" title="Halaman Profil Instagram Anisa"><img src="https://i.pinimg.com/originals/07/62/ea/0762ea4ddb6cd12a7a6b4e6d2513c26f.png" height="40px" width="40px"></a>
			<a class="sosmed" href="https://twitter.com/ncantique17" target="blank" title="Halaman Profil Twitter Anisa"><img src="https://i.pinimg.com/originals/67/c5/65/67c5658a50b21f52e3d283360cfffd3f.png" height="40px" width="40px"></a>
		</center>
	</div>
</div>
@endsection
