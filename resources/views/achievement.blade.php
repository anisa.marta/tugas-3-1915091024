@extends('layout2')

@section('judul')
	Achievement
@endsection

@section('css')
<style>
	.container{
		background-image: url(/img/bg3.png);
		background-size:cover;
		background-repeat: no-repeat;
		padding: 0px 0px 0px;
	}

</style>
@endsection
@section('konten')

<div class="container-contact">
	<div class="container-fluid">
		<div class="container">
			<div class="col-4 offset-2 px-2">
				<img class="img-fluid" src="/img/piagam1.jpeg" alt="">
				<h5 class="text-center mt-2" >JUARA 2</h5>
				<p style="padding-top: 0px" class="text-center" > Pada tahun 2020, saya bergabung dalam Tim RUDAYA untuk mengikuti National Business Plan Competition yang diselenggarakan oleh UKM Kewirausahaan Bursa FEB UNSOED. Alhasil setelah melewati beberapa tahap, tim RUDAYA berhasil meraih JUARA 2 </p>
			</div>
			<div class="col-4 px-2">
				<img class="img-fluid" src="/img/piagam2.png" alt="">
				<h5 class="text-center mt-2" >JUARA 3</h5>
				<p style="padding-top: 0px" class="text-center" > Pada tanggal 18 Desember 2020, saya dan tim meraih juara 3 dalam Lomba Program Kreativitas Mahasiswa serangkaian kegiatan Pagelaran Akhir Tahun Fakultas Teknik dan Kejuruan  </p>
			</div>
		</div>
	</div>
</div>
@endsection
