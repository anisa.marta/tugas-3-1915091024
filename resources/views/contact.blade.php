@extends('layout')

@section('judul')
	Contact
@endsection

@section('konten')
<div class="container-contact">

		
			<div class="contact-box">
				<div class="left"></div>
				<div class="right">
					<h2 class="contact">Contact Me</h2>
					<input type="text" class="field" placeholder="Your Name">
					<input type="text" class="field" placeholder="Your Email">
					<input type="text" class="field" placeholder="Phone">
					<textarea placeholder="Message" class="field"></textarea>
					<button class="btn">Send</button>
				</div>
				
			</div>
	

</div>
@endsection
