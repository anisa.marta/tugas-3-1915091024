<!DOCTYPE html>
<html>
<head>
	<title>Anisa - @yield('judul')</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Jost:wght@400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="/css/welcome.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
	@yield('css')

</head>

<body>
	<header>
		<nav>
			<h1 class="logo">ANISA MARTA</h1>
			<ul>
				<li class="active">
					<a style="text-decoration:none" href="/">Home</a>
				</li>

				<li  class="active">
					<a style="text-decoration:none" href="/about">About</a>
				</li>

				<li class="active">
					<a style="text-decoration:none" href="/achievement">Achievement</a>
				</li>

				<li class="active">
					<a style="text-decoration:none" href="/contact">Contact</a>
				</li>
			</ul>
		</nav>
        @yield('konten')
	</header>
</body>
</html>