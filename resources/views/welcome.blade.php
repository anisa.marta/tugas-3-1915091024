@extends('layout')

@section('judul')
	Home
@endsection

@section('konten')
<div class="container">
	<div class="content">
		<h2>Hai, namaku <span>{{ $name }}</span><br> Mahasiswa dari Universitas Pendidikan Ganesha</h2>
		<p>Motto : "You are more than what you think"<br></p>
	</div>
</div>
@endsection
