<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class,'index'])->name('Home');

Route::get('/about', [App\Http\Controllers\AboutController::class,'index'])->name('About');

Route::get('/achievement', [App\Http\Controllers\AchievementController::class,'index'])->name('Achievement');

Route::get('/contact', [App\Http\Controllers\ContactController::class,'index'])->name('contact');